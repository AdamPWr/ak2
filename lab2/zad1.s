
.global main

SYSEXIT = 60
SYSREAD = 0
SYSWRITE = 1
STDIN = 0
STDOUT = 1
EXIT_SUCCESS = 0



# .data
# string: .ascii "AbCDERjdd."
# string_len = . - string

.bss
.comm string , 100

.stext 
main:

mov $SYSREAD, %rax			# Wczytanie danych ze standardowego wejscia
mov $STDIN, %rdi			# ...funkcja systemowa read
mov $string, %rsi
mov $100, %rdx
syscall

mov %rax, %r15 # dlugosc wczytanego tekstu


mov $0, %rcx # inicjalizaja licznika petli
mov $0, %rax # zeby bylo ladnie w debugerze widac jak sie zmienia

loop:
mov string(,%rcx,1), %al
and $0b11011111, %al
mov %al, string(,%rcx,1)
inc %rcx    # inkrementacja licznika petli
cmp %r15 , %rcx
jne loop



print_result:
mov $SYSWRITE, %rax
mov $STDOUT, %rbx
mov $string, %rsi
mov %r15, %rdx
syscall




end:
mov $SYSEXIT, %rax
mov $EXIT_SUCCESS, %rbx
syscall

