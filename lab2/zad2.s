.global main

SYSEXIT = 60
SYSREAD = 0
SYSWRITE = 1
STDIN = 0
STDOUT = 1
EXIT_SUCCESS = 0

# .data
# string: .ascii "ABcdEf."
# string_len = . - string

.bss
.comm string , 100

.text

main:

mov $SYSREAD, %rax			# Wczytanie danych ze standardowego wejscia
mov $STDIN, %rdi			# ...funkcja systemowa read
mov $string, %rsi
mov $100, %rdx
syscall

mov %rax, %r15 # dlugosc wczytanego tekstu

mov $0, %rcx # wskaznik na pierwszy znak do zamienienia
mov %r15, %rbx # wskaznik na drugi znak do zamienienia
mov %r15, %r8
shr $1, %r8
dec %rbx

mov $0, %rax
mov $0, %rdx

loop:
mov string(,%rbx,1), %al
mov string(,%rcx,1), %dl
mov %al, string(,%rcx,1)
mov %dl, string(,%rbx,1)
inc %rcx    # przesuniecie wskaznika
dec %rbx    # przesuniecie wskaznika
cmp %r8 , %rcx
jne loop


print_result:
mov $SYSWRITE, %rax
mov $STDOUT, %rbx
mov $string, %rsi
mov %r15, %rdx
syscall

end:
mov $SYSEXIT, %rax
mov $EXIT_SUCCESS, %rbx
syscall