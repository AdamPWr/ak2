.global main

SYSREAD = 3
SYSWRITE = 4
STDOUT = 1
STDIN = 0
SYSEXIT = 1
EXIT_SUCCESS = 0

.bss
.comm output, 512

.section .data
number_one: .long 0xFFFFFFFF,0xFFFFFFFB,0xFFFFFFFC,0xFFFFFFFD,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF
number_two: .long 0x00000001,0x00000000,0x00000000,0x00000000,0x00000000,0x00000000,0x00000000,0x00000000

.text
main:
clc     # czysci flage przeniesienia



mov $0, %edx # licznik petli

push eflags

loop:
pop eflags
mov number_one(,%edx,4), %eax
mov number_two(,%edx,4), %ebx
adc %eax, %ebx
push eflags
mov %ebx, output(,%edx,4)
inc %edx
cmp $16, %edx 
jne loop

print_result:
mov $SYSWRITE, %eax
mov $STDOUT, %ebx
mov $output, %ecx
mov $512, %edx
int $0x80

koniec:
mov $SYSEXIT, %eax
mov $EXIT_SUCCESS, %ebx
int $0x80
