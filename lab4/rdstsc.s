.globl rdtsc
.type rdtsc, @function

rdtsc:
pushq %rbp              # Wejscie do funkcji
movq %rsp, %rbp
pushq %rbx              # Zapisanie wartosci rbx na stosie
                        # (zgodnie z konwencja wywolan)

movl $0, %eax           # Wybor Maximum Return Value dla CPUID
# cpuid                   # ostatni apragraf storny 10 dlaczego warto używać
                        # w skrócie cpuid odpowiada za serializacje
                        # dzięki temu przy wywoływaniu rdtsc odejmowany
                        # jest błąd pomiaru 
                        # związany z cyklami zegara
rdtsc                   # edx:eax - time-stamp counter

shl $32, %rdx          # Przesuniecie bitow z edx do wyzszej polowy rdx
or %rdx, %rax          # W rax zwracany wynik (zlozony z edx:eax)

popq %rbx               # Przywrocenie wartosci rbx ze stosu
popq %rbp               # Wyjscie z funkcji
ret
