.data
formatTicks: .ascii "Ticks: %llu\n\0"
formatSeconds: .ascii "Seconds: %llu\n\0"   
formatMinutes: .ascii "Minutes: %llu\n\0"   
.text
.global rdtsc
.type rdtsc, @function

rdtsc:
push %rbp       # ramka stosu
mov %rsp, %rbp
push %rdi
cmp $0, %rdi # sprawdzenie ktora wersje (rdtsp czy rdtscp) mamy wywolac
je zero
jmp jeden

zero:
rdtsc
jmp end
jeden:

rdtscp
end:

shl  $32, %rdx  # przesuwanie bitów wyniku bo jest w 2 rejestrach
xor %rdx, %rax  # scalenie wyniku

mov  $formatTicks, %rdi 
mov  %rax, %rsi
push %rax  # przed printfem trzeba zapisać rax
mov  $0, %eax
call printf
pop %rax # nadmiarowe ale zostawione dlalepszej czytelnosci

push %rax
mov $4000000000, %rbx # częstotliwość taktowania
div %rbx
mov  $formatSeconds, %rdi # printowanie sekund
mov  %rax, %rsi
push %rax
mov  $0, %eax
call printf
pop %rax


push %rax
mov $60, %rbx
div %rbx
mov  $formatMinutes, %rdi
mov  %rax, %rsi
push %rax
mov  $0, %eax
call printf
pop %rax
pop %rax
pop %rax

pop %rdi
leave
ret

